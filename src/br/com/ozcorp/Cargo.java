package br.com.ozcorp;

/**
 * @author Matheus Santos
 *
 */
public class Cargo {
	//atributos
	private String titulo;
	private double SalarioBase;
	
	//construtor
	public Cargo(String titulo, double salarioBase) {
		this.titulo = titulo;
		SalarioBase = salarioBase;
	}

	// getters & setters
	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public double getSalarioBase() {
		return SalarioBase;
	}

	public void setSalarioBase(double salarioBase) {
		SalarioBase = salarioBase;
	}
	 
}	