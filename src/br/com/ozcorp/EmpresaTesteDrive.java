package br.com.ozcorp;

/**
 * @author Matheus Santos
 *
 */
public class EmpresaTesteDrive {

	public static void main(String[] args) {
		// criando um funcionário
		Analista analist = new Analista("Matheus", "123456", "654321", "456789", "matheus@email.com", 
			"123!", TipoSanguineo.O_POSITIVO, Sexo.MASCULINO, new Departamento("Financeiro", "FI", 
						new Cargo("Analista", 2800.00)), 4);
		
		// imprimindo os dados do funcionário
		System.out.println("DADOS DO FUNCIONÁRIO");
		System.out.println("Nome: " + analist.getNome());
		System.out.println("RG: " + analist.getRg());
		System.out.println("CPF: " + analist.getCpf());
		System.out.println("Matrícula: " + analist.getMatricula());
		System.out.println("E-mail: " + analist.getEmail());
		System.out.println("Senha: " + analist.getSenha());
		System.out.println("Cargo: " + analist.getDepartamento().getCargo().getTitulo());
		System.out.println("Salário Base: " + analist.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo: " + analist.getTipoSanguineo().nome);
		System.out.println("Sexo: " + analist.getSexo().nome);
		
		System.out.println(); // PULA LINHA
		
		Diretor diret = new Diretor("Jorge", "789756", "132546", "846596", "jorge@email.com", 
			"456#", TipoSanguineo.A_NEGATIVO, Sexo.MASCULINO, new Departamento("Chefe", "CH",
						new Cargo("Diretor", 8360.00)), 0);
		
		System.out.println("DADOS DO FUNCIONÁRIO");
		System.out.println("Nome: " + diret.getNome());
		System.out.println("RG: " + diret.getRg());
		System.out.println("CPF: " + diret.getCpf());
		System.out.println("Matrícula: " + diret.getMatricula());
		System.out.println("E-mail: " + diret.getEmail());
		System.out.println("Senha: " + diret.getSenha());
		System.out.println("Cargo: " + diret.getDepartamento().getCargo().getTitulo());
		System.out.println("Salário Base: " + diret.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo: " + diret.getTipoSanguineo().nome);
		System.out.println("Sexo: " + diret.getSexo().nome);
		
		System.out.println(); // PULA LINHA
		
		Engenheiro eng = new Engenheiro("Irineu", "856425", "621486", "321684", "irineu@vcnaosabenemeu.com", 
			"564*", TipoSanguineo.AB_POSITIVO, Sexo.MASCULINO, new Departamento("Engenharia", "EG",
						new Cargo("Engenheiro", 4985.00)), 3);
		
		System.out.println("DADOS DO FUNCIONÁRIO");
		System.out.println("Nome: " + eng.getNome());
		System.out.println("RG: " + eng.getRg());
		System.out.println("CPF: " + eng.getCpf());
		System.out.println("Matrícula: " + eng.getMatricula());
		System.out.println("E-mail: " + eng.getEmail());
		System.out.println("Senha: " + eng.getSenha());
		System.out.println("Cargo: " + eng.getDepartamento().getCargo().getTitulo());
		System.out.println("Salário Base: " + eng.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo: " + eng.getTipoSanguineo().nome);
		System.out.println("Sexo: " + eng.getSexo().nome);
		
		System.out.println(); // PULA LINHA
		
		Gerente ger = new Gerente("Xanaina", "103584", "946485", "31565", "xanaina@email.com", "231&",
			TipoSanguineo.B_NEGATIVO, Sexo.FEMININO, new Departamento("Administrativo", "AD", 
					new Cargo("Gerente", 3220.00)), 2);
		
		System.out.println("DADOS DO FUNCIONÁRIO");
		System.out.println("Nome: " + ger.getNome());
		System.out.println("RG: " + ger.getRg());
		System.out.println("CPF: " + ger.getCpf());
		System.out.println("Matrícula: " + ger.getMatricula());
		System.out.println("E-mail: " + ger.getEmail());
		System.out.println("Senha: " + ger.getSenha());
		System.out.println("Cargo: " + ger.getDepartamento().getCargo().getTitulo());
		System.out.println("Salário Base: " + ger.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo: " + ger.getTipoSanguineo().nome);
		System.out.println("Sexo: " + ger.getSexo().nome);
		
		System.out.println(); // PULA LINHA
		
		Secretario sec = new Secretario("Xuliana", "865496", "321599", "316849", "xuliana@gmail.com",
			"648@", TipoSanguineo.O_NEGATIVO, Sexo.OUTRO, new Departamento("Receptivo", "RC", 
					new Cargo("Secretária", 1200.00)), 1);
		
		System.out.println("DADOS DO FUNCIONÁRIO");
		System.out.println("Nome: " + sec.getNome());
		System.out.println("RG: " + sec.getRg());
		System.out.println("CPF: " + sec.getCpf());
		System.out.println("Matrícula: " + sec.getMatricula());
		System.out.println("E-mail: " + sec.getEmail());
		System.out.println("Senha: " + sec.getSenha());
		System.out.println("Cargo: " + sec.getDepartamento().getCargo().getTitulo());
		System.out.println("Salário Base: " + sec.getDepartamento().getCargo().getSalarioBase());
		System.out.println("Tipo Sanguineo: " + sec.getTipoSanguineo().nome);
		System.out.println("Sexo: " + sec.getSexo().nome);		
	}
}