package br.com.ozcorp;

/**
 * @author Matheus Santos
 *
 */
public enum TipoSanguineo {
	A_POSITIVO("A+"),
	A_NEGATIVO("A-"),
	B_POSITIVO("B+"),
	B_NEGATIVO("B-"),
	AB_POSITIVO("AB+"),
	AB_NEGATIVO("AB-"),
	O_POSITIVO("O+"),
	O_NEGATIVO("O-");

	public String nome;
	
	TipoSanguineo(String nome) {
		this.nome = nome;
	}
}