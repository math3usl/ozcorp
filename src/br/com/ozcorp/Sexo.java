package br.com.ozcorp;

/**
 * @author Matheus Santos
 *
 */
public enum Sexo {
	MASCULINO("Masculino"),
	FEMININO("Feminino"),
	OUTRO("Outro");
	
	public String nome;
	
	Sexo(String nome) {
		this.nome = nome;
	}
}