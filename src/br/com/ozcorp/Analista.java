package br.com.ozcorp;

/**
 * @author Matheus Santos
 *
 */
public class Analista extends Funcionario{

	//construtor
	public Analista(String nome, String rg, String cpf, String matricula, String email, String senha,
			TipoSanguineo tipoSanguineo, Sexo sexo, Departamento departamento, int nivelAcesso) {
		super(nome, rg, cpf, matricula, email, senha, tipoSanguineo, sexo, departamento, nivelAcesso);
	}
	
}
